package exebuilder.controller;

import exebuilder.core.Controllers;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.stage.DirectoryChooser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

public class FirstController implements Initializable {

  private static final Logger logger = LogManager.getLogger();
  public TextField appNameField, outputPathField;
  public RadioButton guiRadioButton, consoleRadioButton, springbootRadioButton;


  public void chooseOutputPath(ActionEvent actionEvent) {
    var directoryChooser = new DirectoryChooser();
    File path = directoryChooser.showDialog(null);
    if (path != null) {
      String outputPath = path.getAbsolutePath();
      outputPathField.setText(outputPath);
      logger.debug("output path：{}", outputPath);
    }
  }

  @Override
  public void initialize(URL url, ResourceBundle resourceBundle) {
    Controllers.add("first", this);
    var toggleGroup = new ToggleGroup();
    guiRadioButton.setToggleGroup(toggleGroup);
    consoleRadioButton.setToggleGroup(toggleGroup);
    springbootRadioButton.setToggleGroup(toggleGroup);
    guiRadioButton.setSelected(true);
    springbootRadioButton.setVisible(false);
  }


}
